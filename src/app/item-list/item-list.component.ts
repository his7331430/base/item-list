import {
  Component,
  EventEmitter,
  Input,
  Output,
  inject,
  signal,
} from '@angular/core';
import { CommonModule } from '@angular/common';
import { ItemHeaderComponent } from './item-header/item-header.component';
import { ItemBodyComponent } from './item-body/item-body.component';
import { Item } from './item.interface';
import { ItemService } from './item.service';

@Component({
  selector: 'his-item-list',
  standalone: true,
  imports: [CommonModule, ItemHeaderComponent, ItemBodyComponent],
  templateUrl: './item-list.component.html',
  styleUrls: ['./item-list.component.scss'],
})
export class ItemListComponent {
  @Input() items = signal([] as Item[]);

  @Output() delete: EventEmitter<Item> = new EventEmitter<Item>();

  @Output() update: EventEmitter<Item> = new EventEmitter<Item>();

  itemService: ItemService = inject(ItemService);

  /**
   * 觸發刪除事件
   * @param item
   */
  doDeleteItem(item: Item) {
    this.delete.emit(item);
  }

  /**
   * 觸發更新事件
   * @param item
   */
  doUpdateItem(item: Item) {
    this.update.emit(item);
  }
}
