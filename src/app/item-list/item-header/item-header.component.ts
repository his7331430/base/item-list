import { Component, EventEmitter, Input, Output } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Item } from '../item.interface';
import { ButtonModule } from 'primeng/button';

@Component({
  selector: 'his-item-header',
  standalone: true,
  imports: [CommonModule, ButtonModule],
  templateUrl: './item-header.component.html',
  styleUrls: ['./item-header.component.scss'],
})
export class ItemHeaderComponent {
  @Input() item!: Item;

  @Output() delete: EventEmitter<Item> = new EventEmitter<Item>();

  @Output() insert: EventEmitter<Item> = new EventEmitter<Item>();

  @Output() update: EventEmitter<Item> = new EventEmitter<Item>();

  isEdit = false;

  /**
   * 觸發刪除事件
   * @param item
   */
  doDeleteItem(item: Item) {
    this.delete.emit(item);
  }

  /**
   * 觸發更新事件
   * @param item
   */
  doUpdateItem(item: Item) {
    this.update.emit(item);
  }

  /**
   * 觸發新增事件
   * @param item
   */
  doInsertItem(item: Item) {
    this.insert.emit(item);
  }
}
